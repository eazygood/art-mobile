package com.fitchoice.mobile.object;

public class Admin {

    private static String id, forename, surname, email, password, type;

    public Admin() {

    }

    public static String getId() {
        return id;
    }

    public static void setId(String id) {
        Admin.id = id;
    }

    public static String getForename() {
        return forename;
    }

    public static void setForename(String forename) {
        Admin.forename = forename;
    }

    public static String getSurname() {
        return surname;
    }

    public static void setSurname(String surname) {
        Admin.surname = surname;
    }

    public static String getEmail() {
        return email;
    }

    public static void setEmail(String email) {
        Admin.email = email;
    }

    public static String getPassword() {
        return password;
    }

    public static void setPassword(String password) {
        Admin.password = password;
    }

    public static String getType() {
        return type;
    }

    public static void setType(String type) {
        Admin.type = type;
    }

}

