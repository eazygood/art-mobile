package com.fitchoice.mobile;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.fitchoice.mobile.Task.EventListenerInterface;
import com.fitchoice.mobile.Type.ClientActivity;
import com.fitchoice.mobile.Type.TrainerActivity;
import com.fitchoice.mobile.admin.AdminHomeActivity;
import com.fitchoice.mobile.object.Admin;
import com.fitchoice.mobile.object.Trainer;
import com.fitchoice.mobile.util.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends AppCompatActivity implements EventListenerInterface {


    EditText etEmail, etPassword;
    Button btnLogin;
    TextView btnRegister;
    String email, password;

    static String id, type;

    JSONObject jsonObject;
    JSONArray jsonArray;

    String json_string;
    String jsonStatus;

    Server server = new Server();
    Admin admin = new Admin();

    private JSONObject postData = new JSONObject();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etEmail = (EditText) findViewById(R.id.email);
        etPassword = (EditText) findViewById(R.id.password);

        btnLogin = (Button) findViewById(R.id.btnSignin);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkLogin();
            }
        });

        btnRegister = (TextView) findViewById(R.id.noAccount);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forwardRegistrationForm();
            }
        });
    }

    private void checkLogin() {
        if (etEmail.getText().toString().trim().equals("")) {
            etEmail.setError("Login is required!");

        } else if (etPassword.getText().toString().trim().equals("")) {
            etPassword.setError("Password is required!");
        } else {

            email = etEmail.getText().toString();
            password = etPassword.getText().toString();

            try {
                postData.put("email", email);
                postData.put("password", password);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            MainClient client = new MainClient(postData, server.getLogin(), this);

            client.sendHttpRequest();
        }
    }

    private void forwardRegistrationForm()
    {
        Intent in = new Intent(this, RegistrationActivity.class);
        startActivity(in);
    }

    @Override
    public void onEventCompleted(String result) {
        System.out.println("on_complete" + result);

        try {
            jsonObject = new JSONObject(result);

            jsonStatus = jsonObject.getString("status");

            System.out.println("json4ik" + jsonStatus);
            if (jsonStatus.equals("false")) {
                Toast.makeText(getApplication(), "Incorrect username or password" , Toast.LENGTH_LONG).show();
            } else {
                JSONObject jsonData = jsonObject.getJSONObject("data");
                JSONObject jsonType = jsonData.getJSONObject("type");

                Boolean typeTrainer = jsonType.getBoolean("isTrainer");
                Boolean typeAdmin = jsonType.getBoolean("isRoot");

                String name = jsonType.getString("name");
                String email = jsonType.getString("email");

                if (typeAdmin) {
                    Intent in = new Intent(this, AdminHomeActivity.class);
                    in.putExtra("name", name);
                    in.putExtra("email", email);
                    startActivity(in);
                    return;
                }

                if (typeTrainer) {
                    Intent in = new Intent(this, TrainerActivity.class);
                    in.putExtra("name", name);
                    in.putExtra("email", email);
                    startActivity(in);
                    return;
                }

                Intent in = new Intent(this, ClientActivity.class);
                in.putExtra("name", name);
                in.putExtra("email", email);
                startActivity(in);



            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onEventFailed() {

    }
}