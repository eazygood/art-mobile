package com.fitchoice.mobile.util;

import android.support.v7.app.AppCompatActivity;

public class Server {

    private String serverIP = "http://78.24.221.246";
    private String serverPath = "/fitness/api/";
    private String serverApiVersion = "v1";

    private String serverApiActionPath = serverIP + serverPath + serverApiVersion + "/";

    public enum Action {
        TEST_ACTION ("test_action"),
        LOGIN ("login_action"),
        REGISTER ("register"),
        REGISTER_CLASS ("register_class"),
        SHOW_USER_CLASS ("show_user_class"),
        SHOW_ALL_CLASSES ("show_all_classes"),
        BOOK_CLASS ("book_class"),
        CANCEL_BOOKED_CLASS ("cancel_booked_class"),
        SHOW_USERS("show_users"),
        EDIT_PROFILE("edit_profile"),
        EDIT_CLASS("edit_class")
        ;

        private String value;

        Action(String value) {
            this.value = value;
        }

        public String getActionValue() {
            return value;
        }
    }

    private String testAction = serverApiActionPath + Action.TEST_ACTION.getActionValue();
    private String login = serverApiActionPath + Action.LOGIN.getActionValue();
    private String register = serverApiActionPath + Action.REGISTER.getActionValue();
    private String registerClass = serverApiActionPath + Action.REGISTER_CLASS.getActionValue();
    private String showUserClassAction = serverApiActionPath + Action.SHOW_USER_CLASS.getActionValue();
    private String showAllClassesAction = serverApiActionPath + Action.SHOW_ALL_CLASSES.getActionValue();
    private String bookClassAction = serverApiActionPath + Action.BOOK_CLASS.getActionValue();
    private String cancelBookedClassAction = serverApiActionPath + Action.CANCEL_BOOKED_CLASS.getActionValue();
    private String showUsersAction = serverApiActionPath + Action.SHOW_USERS.getActionValue();
    private String editProfileAction = serverApiActionPath + Action.EDIT_PROFILE.getActionValue();
    private String editClassAction = serverApiActionPath + Action.EDIT_CLASS.getActionValue();

    public Server() {
    }

    public String getTestAction() {
        return testAction;
    }

    public String getLogin() {
        return login;
    }

    public String getRegister() {
        return register;
    }

    public String getRegisterClass() {
        return registerClass;
    }

    public String getShowUserClassAction() {
        return showUserClassAction;
    }

    public String getShowAllClassesAction() {
        return showAllClassesAction;
    }

    public String getBookClassAction() {
        return bookClassAction;
    }

    public String getCancelBookedClassAction() {
        return cancelBookedClassAction;
    }

    public String getShowUsersAction() {
        return showUsersAction;
    }

    public String getEditProfileAction() {
        return editProfileAction;
    }

    public String getEditClassAction() {
        return editClassAction;
    }
}
