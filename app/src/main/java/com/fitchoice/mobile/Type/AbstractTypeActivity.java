package com.fitchoice.mobile.Type;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import com.fitchoice.mobile.R;
import com.fitchoice.mobile.Task.EventListenerInterface;
import com.fitchoice.mobile.util.Server;

import org.json.JSONObject;

import java.util.Calendar;

abstract public class AbstractTypeActivity extends AppCompatActivity implements EventListenerInterface {

    Boolean proceedForm = false;
    JSONObject postData;

    TextView welcomeView;
    String userEmail;

    Server server = new Server();

    EditText time, date;
    protected int mYear, mMonth, mDay, mHour, mMinute;

    static final String GENDER_CHAR_MALE = "m";
    static final String GENDER_MALE = "Male";
    static final String GENDER_CHAR_FEMALE = "f";
    static final String GENDER_FEMALE = "Female";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (null != getWelcomeView()) {
            setWelcomeTextView();
        }

        setUserEmail();

    }

    protected void setWelcomeTextView() {
        TextView welcomeView = getWelcomeView();

        String value = getIntent().getExtras() != null ?  getIntent().getExtras().getString("name") : "";
        System.out.println("WELCOME_TEXT " +value);
        System.out.println("EMAIL " + getUserEmail());
        welcomeView.setText(value);
    }

    protected TextView getWelcomeView()
    {
        return welcomeView;
    }

    protected void setUserEmail() {}

    protected String getUserEmail() {
        return getIntent().getExtras() != null ?  getIntent().getExtras().getString("email") : "";
    }

    protected Boolean preRegistrationCheck() {
        return proceedForm;
    }

    protected void isEmpty(EditText text) {

        if (text.getText().toString().trim().equals("")) {
            text.setError("Required field");
            proceedForm = false;
        } else {
            proceedForm = true;
        }
    }

    protected JSONObject generatePostData() {
        return postData;
    }

    protected void onRegister() {}

    protected void onClickPickTime() {
        final Calendar calendar = Calendar.getInstance();
        mHour = calendar.get(Calendar.HOUR_OF_DAY);
        mMinute = calendar.get(Calendar.MINUTE);

        // Launch TimePicker
        TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String pickedTime = hourOfDay + ":" + minute;
                getTimeEditText().setText(pickedTime);
            }
        }, mHour, mMinute, false);

        timePickerDialog.show();
    }

    protected void onClickPickDate() {
        final Calendar calendar = Calendar.getInstance();
        mYear = calendar.get(Calendar.YEAR);
        mMonth = calendar.get(Calendar.MONTH);
        mDay = calendar.get(Calendar.DAY_OF_MONTH);

        // Launch TimePicker
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String pickedDate = dayOfMonth + "-" + (month + 1) + "-" + year;
                getDateEditText().setText(pickedDate);
            }
        }, mYear, mMonth, mDay);

        datePickerDialog.show();
    }

    protected EditText getTimeEditText() { return time; }
    protected EditText getDateEditText() { return date; }

    public void setTime(EditText time) {
        this.time = time;
    }

    public void setDate(EditText date) {
        this.date = date;
    }

    public Server getServer() {
        return server;
    }

    protected String getGenderFullText(String c)
    {

        String gender = c.trim().toLowerCase();

        if (gender.equals(GENDER_CHAR_MALE)) {
            return GENDER_MALE;
        }

        if (gender.equals(GENDER_CHAR_FEMALE)) {
            return GENDER_FEMALE;
        }

        return "";
    }
}
