package com.fitchoice.mobile.Type;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.fitchoice.mobile.MainClient;
import com.fitchoice.mobile.R;

import org.json.JSONException;
import org.json.JSONObject;

public class ClientBookClassListEditActivity  extends BaseClassListEditActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_client_book_class);
        classId = getIntent().getExtras() != null ?  getIntent().getExtras().getString("class_id") : "";
        userEmail = getUserEmail();
        System.out.println("UUUUUSER" + getUserEmail());

        String className = getIntent().getExtras() != null ?  getIntent().getExtras().getString("class_name") : "";
        String location = getIntent().getExtras() != null ?  getIntent().getExtras().getString("location") : "";
        String time = getIntent().getExtras() != null ?  getIntent().getExtras().getString("time") : "";
        String date = getIntent().getExtras() != null ?  getIntent().getExtras().getString("date") : "";

        editClassNameTxt = (EditText) findViewById(R.id.bookClientClassname);
        editClassNameTxt.setText(className);
        editLocationTxt = (EditText) findViewById(R.id.bookClientLocation);
        editLocationTxt.setText(location);
        editTimeTxt = (EditText) findViewById(R.id.bookClientTime);
        editTimeTxt.setText(time);
        editTimeTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickPickTime();
            }
        });
        editDateTxt = (EditText) findViewById(R.id.bookClientDate);
        editDateTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickPickDate();
            }
        });

        editDateTxt.setText(date);

        btnSaveEditedClass = findViewById(R.id.btnBookClientClass);
        btnSaveEditedClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (preRegistrationCheck()) {
                    onRegister();
                } else {
                    Toast.makeText(getApplication(), "Data not registered" , Toast.LENGTH_SHORT).show();
                }
            }
        });
        super.onCreate(savedInstanceState);

    }

    @Override
    protected JSONObject generatePostData() {
        postData = new JSONObject();
        try {
            postData.put("class_id", classId);
            postData.put("user_email", userEmail);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return postData;
    }

    @Override
    protected void onRegister() {
        MainClient mainClient = new MainClient(generatePostData(), server.getBookClassAction(), this);
        mainClient.sendHttpRequest();
    }

    @Override
    protected String onSuccessToastMessage() {
        return "Data saved";
    }
}
