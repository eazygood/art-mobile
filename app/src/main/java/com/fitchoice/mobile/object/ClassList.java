package com.fitchoice.mobile.object;

import java.util.ArrayList;

public class ClassList {

    private String classId;

    private String className;

    private String location;

    private String time;

    private String date;

    public ClassList() {
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public static ArrayList<ClassList> createContactsList(int num) {
        ArrayList<ClassList> classListArrayList = new ArrayList<ClassList>();

        for (int i = 1; i <= num; i++) {
            ClassList classList = new ClassList();
            classList.setClassName("Classname" + i);
            classListArrayList.add(classList);
        }

        return classListArrayList;
    }
}
