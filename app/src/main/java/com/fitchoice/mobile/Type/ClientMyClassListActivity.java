package com.fitchoice.mobile.Type;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.fitchoice.mobile.Adapter.ClassListAdapter;
import com.fitchoice.mobile.MainClient;
import com.fitchoice.mobile.R;
import com.fitchoice.mobile.object.ClassList;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ClientMyClassListActivity extends BaseClassListActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setForwardOnEditClassIntent(new Intent(this, ClientMyClassListEditActivity.class));

        classListArrayList = new ArrayList<ClassList>();

        setContentView(R.layout.activity_client_class_list);

        RecyclerView recyclerView = findViewById(R.id.recyclerViewClassClientMyClass);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        classListAdapter = new ClassListAdapter(this, classListArrayList);
        classListAdapter.setClickListener(this);
        recyclerView.setAdapter(classListAdapter);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), linearLayoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("ON RESUME");
        System.out.println("NOTIIIIIIIIIIIIIFY ");
        retrieveData();
    }

    protected void retrieveData() {
        MainClient mainClient = new MainClient(generatePostData(), server.getShowUserClassAction(), this);
        mainClient.sendHttpRequest();
    }

    @Override
    protected JSONObject generatePostData() {
        postData = new JSONObject();
        try {
            postData.put("user_email", getUserEmail());
            postData.put("booked_class", true);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return postData;
    }
}
