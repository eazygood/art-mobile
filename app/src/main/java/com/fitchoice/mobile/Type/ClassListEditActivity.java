package com.fitchoice.mobile.Type;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.fitchoice.mobile.MainClient;
import com.fitchoice.mobile.R;

import org.json.JSONException;
import org.json.JSONObject;

public class ClassListEditActivity extends AbstractTypeActivity {

    EditText editClassNameTxt, editLocationTxt, editTimeTxt, editDateTxt;
    Button btnSaveEditedClass;

    String classId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_trainer_edit_class);
        classId = getIntent().getExtras() != null ?  getIntent().getExtras().getString("class_id") : "";
        userEmail = getUserEmail();
        System.out.println("UUUUUSER" + getUserEmail());

        String className = getIntent().getExtras() != null ?  getIntent().getExtras().getString("class_name") : "";
        String location = getIntent().getExtras() != null ?  getIntent().getExtras().getString("location") : "";
        String time = getIntent().getExtras() != null ?  getIntent().getExtras().getString("time") : "";
        String date = getIntent().getExtras() != null ?  getIntent().getExtras().getString("date") : "";

        editClassNameTxt = (EditText) findViewById(R.id.editClassName);
        editClassNameTxt.setText(className);
        editLocationTxt = (EditText) findViewById(R.id.editLocation);
        editLocationTxt.setText(location);
        editTimeTxt = (EditText) findViewById(R.id.editTime);
        editTimeTxt.setText(time);
        editTimeTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickPickTime();
            }
        });
        editDateTxt = (EditText) findViewById(R.id.editDate);
        editDateTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickPickDate();
            }
        });
        editDateTxt.setText(date);

        btnSaveEditedClass = findViewById(R.id.btnSaveEditedClass);
        btnSaveEditedClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (preRegistrationCheck()) {
                    System.out.println("HUJ" + generatePostData());
                    onRegister();
                } else {

                }
            }
        });
        super.onCreate(savedInstanceState);

    }

    @Override
    protected Boolean preRegistrationCheck() {
        isEmpty(editClassNameTxt);
        isEmpty(editLocationTxt);
        isEmpty(editTimeTxt);
        isEmpty(editDateTxt);

        return proceedForm;
    }

    @Override
    protected JSONObject generatePostData() {
        postData = new JSONObject();
        try {
            postData.put("class_id", classId);
            postData.put("user_email", userEmail);
            postData.put("class_name", editClassNameTxt.getText().toString().trim());
            postData.put("location", editLocationTxt.getText().toString().trim());
            postData.put("time", editTimeTxt.getText().toString().trim());
            postData.put("date", editDateTxt.getText().toString().trim());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return postData;
    }

    @Override
    protected void onRegister() {
        MainClient mainClient = new MainClient(generatePostData(), server.getRegisterClass(), this);
        mainClient.sendHttpRequest();
    }

    @Override
    public void onEventCompleted(String result) {
        System.out.println("on_complete" + result);

        try {
            JSONObject jsonObject = new JSONObject(result);

            String jsonStatus;
            jsonStatus = jsonObject.getString("status");

            System.out.println("json4ik" + jsonStatus);
            if (jsonStatus.equals("false")) {
                JSONObject jsonData = jsonObject.getJSONObject("data");
                System.out.println(jsonData.getString("message"));
                Toast.makeText(getApplication(), jsonData.getString("message") , Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplication(), "Data updated" , Toast.LENGTH_LONG).show();

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onEventFailed() {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    @Override
    protected EditText getTimeEditText() {
        return editTimeTxt;
    }

    @Override
    protected EditText getDateEditText() {
        return editDateTxt;
    }
}
