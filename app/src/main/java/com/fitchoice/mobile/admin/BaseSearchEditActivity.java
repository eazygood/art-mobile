package com.fitchoice.mobile.admin;


import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.fitchoice.mobile.MainClient;
import com.fitchoice.mobile.Type.AbstractTypeActivity;
import com.fitchoice.mobile.util.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BaseSearchEditActivity extends AbstractTypeActivity {


    Server server = new Server();


    protected void onConfirmChanges() {
        MainClient mainClient = new MainClient(generatePostData(), server.getEditProfileAction(), this);
        mainClient.sendHttpRequest();
    }

    @Override
    public void onEventCompleted(String result) {
        System.out.println("on_complete" + result);

        try {
            JSONObject jsonObject = new JSONObject(result);

            String jsonStatus;
            jsonStatus = jsonObject.getString("status");

            System.out.println("json4ik" + jsonStatus);

            if (jsonStatus.equals("false")) {
                JSONObject jsonData = jsonObject.getJSONObject("data");

                System.out.println(jsonData.getString("message"));
                Toast.makeText(getApplication(), jsonData.getString("message") , Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplication(), onSuccessToastMessage(), Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onEventFailed() {

    }

    protected void forwardBack() {
        finish();
    }


    protected String onSuccessToastMessage() {
        return "Data updated";
    }
}
