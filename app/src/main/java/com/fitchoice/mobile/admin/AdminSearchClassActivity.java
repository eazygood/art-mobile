package com.fitchoice.mobile.admin;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.fitchoice.mobile.Adapter.ClassListAdapter;
import com.fitchoice.mobile.MainClient;
import com.fitchoice.mobile.R;
import com.fitchoice.mobile.Type.BaseClassListActivity;
import com.fitchoice.mobile.Type.ClientMyClassListEditActivity;
import com.fitchoice.mobile.object.ClassList;
import com.fitchoice.mobile.util.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AdminSearchClassActivity extends BaseClassListActivity {

    ArrayList<ClassList> classListArrayList;

    ClassListAdapter classListAdapter;
    LinearLayoutManager linearLayoutManager;
    JSONObject postData;

    EditText searchClassTxt;

    Server server = new Server();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        setContentView(R.layout.activity_admin_search_class);

        setForwardOnEditClassIntent(new Intent(this, AdminEditClassActivity.class));
        classListArrayList = new ArrayList<ClassList>();

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerViewAdminClass);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        classListAdapter = new ClassListAdapter(this, classListArrayList);
        setClassListAdapter(classListAdapter);

        classListAdapter.setClickListener(this);
        recyclerView.setAdapter(classListAdapter);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), linearLayoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        searchClassTxt = (EditText) findViewById(R.id.searchAdminClass);
        searchClassTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filterSearchItem(s.toString());
            }
        });

        super.onCreate(savedInstanceState);

    }

    protected void forwardOnEditClass(int position) {
        Intent in = this.getForwardOnEditClassIntent();

        ClassList classList = classListAdapter.getClassListItem(position);
        in.putExtra("email", getUserEmail());
        in.putExtra("class_id", classList.getClassId());
        in.putExtra("class_name", classList.getClassName());
        in.putExtra("location", classList.getLocation());
        in.putExtra("time", classList.getTime());
        in.putExtra("date", classList.getDate());

        startActivity(in);
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("ON RESUME");
        System.out.println("NOTIIIIIIIIIIIIIFY ");
        retrieveData();
    }

    protected void retrieveData() {
        MainClient mainClient = new MainClient(generatePostData(), server.getShowAllClassesAction(), this);
        mainClient.sendHttpRequest();
    }

    @Override
    protected JSONObject generatePostData() {
        postData = new JSONObject();
        try {
            postData.put("user_email", getUserEmail());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return postData;
    }

       protected void filterSearchItem(String text) {
        ArrayList<ClassList> filteredList = new ArrayList<>();

        for (ClassList item : classListArrayList) {
            if (item.getClassName().toLowerCase().contains(text.toLowerCase())) {
                filteredList.add(item);
            }
        }

        classListAdapter.filterList(filteredList);
    }
}
