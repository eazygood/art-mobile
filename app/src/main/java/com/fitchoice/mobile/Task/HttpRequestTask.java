package com.fitchoice.mobile.Task;

import android.os.AsyncTask;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class HttpRequestTask extends AsyncTask<String, String , String> {

    private JSONObject postData;

    private HttpURLConnection urlConnection = null;
    private StringBuffer response = new StringBuffer();

    private String responseText;
    private EventListenerInterface cb;


    public HttpRequestTask(JSONObject postData, EventListenerInterface cb) {
        if (postData != null) {
            this.postData = postData;
        }
        this.cb = cb;

    }

    public String getResponseText() {
        return responseText;
    }

    @Override
    protected String doInBackground(String... params) {

        // Here will be post to server
        try {
            URL url = new URL(params[0]);

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestMethod("POST");
            urlConnection.setConnectTimeout(5000);

            // send post body
            DataOutputStream dataOutputStream = new DataOutputStream(urlConnection.getOutputStream());
            dataOutputStream.writeBytes(postData.toString());
            dataOutputStream.flush();
            dataOutputStream.close();

            if (urlConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                response.append("HTTP Response code: ").append(urlConnection.getResponseCode());
            }

            InputStream inputStream = urlConnection.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                response.append(line);
                response.append('\n');
            }

            bufferedReader.close();


        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        System.out.println("respon4ik "  + response.toString());
        return response.toString();
    }

    @Override
    protected void onPostExecute(String result) {
        System.out.println("resul4ik " + result);
        cb.onEventCompleted(result);
    }


}
