package com.fitchoice.mobile.admin;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.fitchoice.mobile.MainClient;
import com.fitchoice.mobile.R;
import com.fitchoice.mobile.Type.AbstractTypeActivity;
import com.fitchoice.mobile.util.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class AdminEditClientActivity extends BaseSearchEditActivity {

    String userEmail;

    EditText nameTxt, surnameTxt, ageTxt, genderTxt, emailTxt;
    Button btnAdminClientConfirm, btnAdminClientCancel;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_admin_edit_client);
        userEmail = getUserEmail();

        String name = getIntent().getExtras() != null ?  getIntent().getExtras().getString("name") : "";
        String surName = getIntent().getExtras() != null ?  getIntent().getExtras().getString("surname") : "";
        String age = getIntent().getExtras() != null ?  getIntent().getExtras().getString("age") : "";
        String gender = getIntent().getExtras() != null ?  getIntent().getExtras().getString("gender") : "";
        String email = getIntent().getExtras() != null ?  getIntent().getExtras().getString("user_email") : "";

        nameTxt = (EditText) findViewById(R.id.clientName);
        nameTxt.setText(name);

        surnameTxt = (EditText) findViewById(R.id.clientSurname);
        surnameTxt.setText(surName);

        ageTxt = (EditText) findViewById(R.id.clientAge);
        ageTxt.setText(age);

        genderTxt = (EditText) findViewById(R.id.clientGender);
        genderTxt.setText(gender);

        emailTxt = (EditText) findViewById(R.id.clientEmail);
        emailTxt.setText(email);
        emailTxt.setEnabled(false);
        emailTxt.setFocusable(false);

        btnAdminClientConfirm = (Button) findViewById(R.id.btnAdminClientConfirm);
        btnAdminClientConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onConfirmChanges();
            }
        });

        btnAdminClientCancel = (Button) findViewById(R.id.btnAdminClientCancel);
        btnAdminClientCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forwardBack();
            }
        });

        super.onCreate(savedInstanceState);

    }

    @Override
    protected JSONObject generatePostData() {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("email", emailTxt.getText());
            jsonObject.put("first_name", nameTxt.getText());
            jsonObject.put("last_name", surnameTxt.getText());
            jsonObject.put("age", ageTxt.getText());
            jsonObject.put("gender", genderTxt.getText());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }
}
