package com.fitchoice.mobile.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fitchoice.mobile.R;
import com.fitchoice.mobile.object.ClassList;
import com.fitchoice.mobile.object.Profile;

import java.util.ArrayList;
import java.util.List;

public class ClassListAdapter extends RecyclerView.Adapter<ClassListAdapter.ViewHolder> {


    private List<ClassList> mClasslists;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    public ClassListAdapter(Context context, List<ClassList> classLists) {
        mClasslists = classLists;
        mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        // Inflate the custom layout
        View view = mInflater.inflate(R.layout.recycle_view_row_class, viewGroup, false);

        // Return a new holder instance
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ClassListAdapter.ViewHolder viewHolder, int position) {
        ClassList classList = mClasslists.get(position);

        TextView textView = viewHolder.nameClass;
        textView.setText(classList.getClassName());
    }

    @Override
    public int getItemCount() {
        return mClasslists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView nameClass;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            nameClass = (TextView) itemView.findViewById(R.id.classNameItem);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            int position = getAdapterPosition();
            if (mClickListener != null) mClickListener.onItemClick(view, position);
        }
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public ClassList getClassListItem(int id) {
        return mClasslists.get(id);
    }

    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public void updateList(List<ClassList> mClasslist) {
        this.mClasslists.clear();
        this.mClasslists.addAll(mClasslist);
    }

    public void filterList(ArrayList<ClassList> classLists) {
        this.mClasslists = classLists;
        notifyDataSetChanged();
    }


}

