package com.fitchoice.mobile.admin;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.fitchoice.mobile.MainClient;
import com.fitchoice.mobile.R;

import org.json.JSONException;
import org.json.JSONObject;

public class AdminEditClassActivity extends BaseSearchEditActivity {

    EditText classAdminClassTxt, locationAdminClassTxt, timeAdminClassTxt, dateAdminClassTxt;
    Button btnClassAdminConfirm, btnClassAdminDelete, btnClassAdminCancel;

    String classId;
    String userEmail;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        setContentView(R.layout.activity_admin_edit_class);

        classId = getIntent().getExtras() != null ?  getIntent().getExtras().getString("class_id") : "";
        userEmail = getUserEmail();
        System.out.println("UUUUUSER" + getUserEmail());

        String className = getIntent().getExtras() != null ?  getIntent().getExtras().getString("class_name") : "";
        String location = getIntent().getExtras() != null ?  getIntent().getExtras().getString("location") : "";
        String time = getIntent().getExtras() != null ?  getIntent().getExtras().getString("time") : "";
        String date = getIntent().getExtras() != null ?  getIntent().getExtras().getString("date") : "";

        classAdminClassTxt = (EditText) findViewById(R.id.classAdminClass);
        classAdminClassTxt.setText(className);

        locationAdminClassTxt = (EditText) findViewById(R.id.locationAdminClass);
        locationAdminClassTxt.setText(location);

        timeAdminClassTxt = (EditText) findViewById(R.id.timeAdminClass);
        timeAdminClassTxt.setText(time);
        timeAdminClassTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickPickTime();
            }
        });
        dateAdminClassTxt = (EditText) findViewById(R.id.dateAdminClass);
        dateAdminClassTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickPickDate();
            }
        });
        dateAdminClassTxt.setText(date);

        btnClassAdminConfirm = findViewById(R.id.btnAdminClassConfirm);
        btnClassAdminConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onConfirmChanges();

            }
        });

        btnClassAdminDelete = findViewById(R.id.btnAdminClassDelete);
        btnClassAdminDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDelete();
            }
        });

        btnClassAdminCancel = findViewById(R.id.btnAdminClassCancel);
        btnClassAdminCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forwardBack();
            }
        });

        setTime(timeAdminClassTxt);
        setDate(dateAdminClassTxt);

        super.onCreate(savedInstanceState);
    }

    @Override
    protected JSONObject generatePostData() {
        JSONObject jsonObject = new JSONObject();

        try {

            jsonObject.put("class_id", Integer.parseInt(classId));
            jsonObject.put("user_email", getUserEmail());
            jsonObject.put("class_name", classAdminClassTxt.getText());
            jsonObject.put("location", locationAdminClassTxt.getText());
            jsonObject.put("time", timeAdminClassTxt.getText());
            jsonObject.put("date", dateAdminClassTxt.getText());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    protected void onConfirmChanges() {
        MainClient mainClient = new MainClient(generatePostData(), server.getEditClassAction(), this);
        mainClient.sendHttpRequest();
    }

    protected void onDelete() {
        JSONObject jsonObject = generatePostData();
        try {
            jsonObject.put("is_active", false);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        MainClient mainClient = new MainClient(generatePostData(), server.getEditClassAction(), this);
        mainClient.sendHttpRequest();
    }
}
