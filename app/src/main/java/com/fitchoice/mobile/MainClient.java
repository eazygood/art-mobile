package com.fitchoice.mobile;

import android.content.Context;

import com.fitchoice.mobile.Task.EventListenerInterface;
import com.fitchoice.mobile.Task.HttpRequestTask;

import org.json.JSONObject;

import java.util.Map;

public class MainClient implements ClientInterface {

    private JSONObject postData;
    private String urlPath;
    private Context context;

    private EventListenerInterface cb;

    public MainClient(JSONObject postData, String urlPath, EventListenerInterface cb) {
        this.postData = postData;
        this.urlPath = urlPath;
        this.cb = cb;
    }

    @Override
    public void sendHttpRequest() {
        HttpRequestTask httpRequestTask = new HttpRequestTask(postData, cb);
        httpRequestTask.execute(urlPath);
    }


}
