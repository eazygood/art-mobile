package com.fitchoice.mobile.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fitchoice.mobile.R;
import com.fitchoice.mobile.object.Profile;

import java.util.ArrayList;
import java.util.List;

public class ProfileListAdapter extends RecyclerView.Adapter<ProfileListAdapter.ViewHolder> {

    private List<Profile> mProfileList;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    public ProfileListAdapter(Context context, List<Profile> profiles ) {
        mInflater = LayoutInflater.from(context);
        mProfileList = profiles;
    }

    @NonNull
    @Override
    public ProfileListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = mInflater.inflate(R.layout.recycle_view_row_profile, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProfileListAdapter.ViewHolder viewHolder, int position) {
        Profile profile = mProfileList.get(position);

        TextView textViewUseName = viewHolder.userName;
        textViewUseName.setText(profile.getName());
//        TextView textViewUserEmail = viewHolder.userName;
//        textViewUserEmail.setText(profile.getEmail());
    }

    @Override
    public int getItemCount() {
        return mProfileList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView userName;
        TextView userEmail;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            userName = (TextView) itemView.findViewById(R.id.userName);
//            userEmail = (TextView) itemView.findViewById(R.id.userEmail);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            if (null != mClickListener) {
                mClickListener.onItemClick(v, position);
            }
        }
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public Profile getProfileItem(int id) {
        return mProfileList.get(id);
    }

    public void setClickListener(ItemClickListener mClickListener) {
        this.mClickListener = mClickListener;
    }

    public void updateList(List<Profile> mProfile) {
        this.mProfileList.clear();
        this.mProfileList.addAll(mProfile);
    }

    public void filterList(ArrayList<Profile> mProfile) {
        this.mProfileList = mProfile;
        notifyDataSetChanged();
    }

}
