package com.fitchoice.mobile.Type;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.fitchoice.mobile.MainClient;
import com.fitchoice.mobile.R;

import org.json.JSONException;
import org.json.JSONObject;

public class ClientActivity extends AbstractTypeActivity {

    Button classListBtn;
    Button createClassBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_client_home);

        userEmail = getUserEmail();

        classListBtn = (Button) findViewById(R.id.btnClientMyClasses);
        classListBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forwardReviewClassList();
            }
        });

        createClassBtn = (Button) findViewById(R.id.btnClientBookClass);
        createClassBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forwardCreateClass();
            }
        });

        super.onCreate(savedInstanceState);
    }

    @Override
    protected TextView getWelcomeView() {
        return findViewById(R.id.welcomeNameClient);
    }

    protected void setUserEmail() {
        userEmail = getIntent().getExtras() != null ?  getIntent().getExtras().getString("email") : "";
    }

    private void forwardReviewClassList() {
        Intent in = new Intent(this, ClientMyClassListActivity.class);
        in.putExtra("email", userEmail);
        startActivity(in);
    }

    protected void forwardCreateClass() {
        Intent in = new Intent(this, ClientBookClassListActivity.class);
        in.putExtra("email", userEmail);
        startActivity(in);
    }

    protected void retrieveData() {
        MainClient mainClient = new MainClient(generatePostData(), server.getShowUserClassAction(), this);
        mainClient.sendHttpRequest();
    }

    @Override
    protected JSONObject generatePostData() {
        postData = new JSONObject();
        try {
            postData.put("user_email", userEmail);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return postData;
    }

    @Override
    public void onEventCompleted(String result) {
        System.out.println("on_complete" + result);

        try {
            JSONObject jsonObject = new JSONObject(result);

            String jsonStatus;
            jsonStatus = jsonObject.getString("status");

            System.out.println("json4ik" + jsonStatus);
            JSONObject jsonData = jsonObject.getJSONObject("data");

            if (jsonStatus.equals("false")) {
                System.out.println(jsonData.getString("message"));
                Toast.makeText(getApplication(), jsonData.getString("message") , Toast.LENGTH_LONG).show();
            } else {
//                forwardClassList();
                Intent in = new Intent(this, ClassListActivity.class);
                in.putExtra("data", jsonData.getString("class"));
                in.putExtra("email", userEmail);
                startActivity(in);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onEventFailed() {

    }
}
