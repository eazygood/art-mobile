package com.fitchoice.mobile.Type;

import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class BaseClassListEditActivity extends AbstractTypeActivity {
    EditText editClassNameTxt, editLocationTxt, editTimeTxt, editDateTxt;
    Button btnSaveEditedClass;

    String classId;

    @Override
    protected EditText getTimeEditText() {
        return editTimeTxt;
    }

    @Override
    protected EditText getDateEditText() {
        return editDateTxt;
    }


    @Override
    protected Boolean preRegistrationCheck() {
        isEmpty(editClassNameTxt);
        isEmpty(editLocationTxt);
        isEmpty(editTimeTxt);
        isEmpty(editDateTxt);

        return proceedForm;
    }

    @Override
    public void onEventCompleted(String result) {
        System.out.println("on_complete" + result);

        try {
            JSONObject jsonObject = new JSONObject(result);

            String jsonStatus;
            jsonStatus = jsonObject.getString("status");

            System.out.println("json4ik" + jsonStatus);
            if (jsonStatus.equals("false")) {
                JSONObject jsonData = jsonObject.getJSONObject("data");
                System.out.println(jsonData.getString("message"));
                Toast.makeText(getApplication(), jsonData.getString("message") , Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplication(), onSuccessToastMessage() , Toast.LENGTH_LONG).show();

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onEventFailed() {

    }

    protected String onSuccessToastMessage() {
        return "Data updated";
    }
}
