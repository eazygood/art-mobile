package com.fitchoice.mobile.Type;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import com.fitchoice.mobile.MainClient;
import com.fitchoice.mobile.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

public class ClassActivity extends AbstractTypeActivity
{
    EditText fitnessClass, time, location, date;

    private int mYear, mMonth, mDay, mHour, mMinute;

    Button createClassBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        setContentView(R.layout.activity_trainer_create_class);

        fitnessClass = (EditText) findViewById(R.id.classCreateTypeTrainer);
        location = (EditText) findViewById(R.id.locationCreateTypeTrainer);
        time = (EditText) findViewById(R.id.timeCreateTypeTrainer);
        date = (EditText) findViewById(R.id.dateCreateTypeTrainer);

        userEmail = getUserEmail();

        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickPickTime();
            }
        });

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickPickDate();
            }
        });

        createClassBtn = (Button) findViewById(R.id.btnCreateClassActionTrainer);
        createClassBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (preRegistrationCheck()) {
                    System.out.println("PROCEEEEEEEEEEEEED" + generatePostData().toString());
                    onRegister();
                } else {
                    System.out.println("NOT PROCEEEEEEEEEEEED");
                }
            }
        });


        super.onCreate(savedInstanceState);
    }


    protected void onClickPickTime() {
        final Calendar calendar = Calendar.getInstance();
        mHour = calendar.get(Calendar.HOUR_OF_DAY);
        mMinute = calendar.get(Calendar.MINUTE);

        // Launch TimePicker
        TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String pickedTime = hourOfDay + ":" + minute;
                time.setText(pickedTime);
            }
        }, mHour, mMinute, false);

        timePickerDialog.show();
    }

    protected void onClickPickDate() {
        final Calendar calendar = Calendar.getInstance();
        mYear = calendar.get(Calendar.YEAR);
        mMonth = calendar.get(Calendar.MONTH);
        mDay = calendar.get(Calendar.DAY_OF_MONTH);

        // Launch TimePicker
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String pickedDate = dayOfMonth + "-" + (month + 1) + "-" + year;
                date.setText(pickedDate);
            }
        }, mYear, mMonth, mDay);

        datePickerDialog.show();
    }

    @Override
    protected Boolean preRegistrationCheck() {
        isEmpty(fitnessClass);
        isEmpty(location);
        isEmpty(time);
        isEmpty(date);

        return proceedForm;
    }

    @Override
    protected void onRegister() {
        MainClient mainClient = new MainClient(generatePostData(), server.getRegisterClass(), this);
        mainClient.sendHttpRequest();
    }

    @Override
    protected JSONObject generatePostData() {
        postData = new JSONObject();
        try {
            postData.put("class_id", null);
            postData.put("user_email", userEmail);
            postData.put("class_name", fitnessClass.getText().toString().trim());
            postData.put("location", location.getText().toString().trim());
            postData.put("time", time.getText().toString().trim());
            postData.put("date", date.getText().toString().trim());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return postData;
    }

    @Override
    public void onEventCompleted(String result) {
        System.out.println("on_complete" + result);

        try {
            JSONObject jsonObject = new JSONObject(result);

            String jsonStatus;
            jsonStatus = jsonObject.getString("status");

            System.out.println("json4ik" + jsonStatus);
            if (jsonStatus.equals("false")) {
                JSONObject jsonData = jsonObject.getJSONObject("data");
                System.out.println(jsonData.getString("message"));
                Toast.makeText(getApplication(), jsonData.getString("message") , Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplication(), "Data created" , Toast.LENGTH_LONG).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onEventFailed() {

    }
}
