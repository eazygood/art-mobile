package com.fitchoice.mobile.Type;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.fitchoice.mobile.Adapter.ClassListAdapter;
import com.fitchoice.mobile.MainClient;
import com.fitchoice.mobile.R;
import com.fitchoice.mobile.object.ClassList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ClientBookClassListActivity extends AbstractTypeActivity implements ClassListAdapter.ItemClickListener {

    ArrayList<ClassList> classListArrayList;
    ClassListAdapter classListAdapter;
    LinearLayoutManager linearLayoutManager;
    JSONArray jsonArray;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client_book_class_list);
        classListArrayList = new ArrayList<ClassList>();

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerViewClassClientBook);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        classListAdapter = new ClassListAdapter(this, classListArrayList);
        classListAdapter.setClickListener(this);
        recyclerView.setAdapter(classListAdapter);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), linearLayoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

    }

    @Override
    public void onItemClick(View view, int position) {
        Toast.makeText(this, "You clicked  on row number " + position, Toast.LENGTH_SHORT).show();
        forwardOnEditClass(position);
    }

    protected void forwardOnEditClass(int position) {
        Intent in = new Intent(this, ClientBookClassListEditActivity.class);

        ClassList classList = classListAdapter.getClassListItem(position);
        in.putExtra("email", getUserEmail());
        in.putExtra("class_id", classList.getClassId());
        in.putExtra("class_name", classList.getClassName());
        in.putExtra("location", classList.getLocation());
        in.putExtra("time", classList.getTime());
        in.putExtra("date", classList.getDate());

        startActivity(in);
    }

    @Override
    public void onEventCompleted(String result) {

        try {
            JSONObject jsonObject = new JSONObject(result);

            String jsonStatus;
            jsonStatus = jsonObject.getString("status");

            JSONObject jsonData = jsonObject.getJSONObject("data");

            if (jsonStatus.equals("false")) {
                System.out.println(jsonData.getString("message"));
                Toast.makeText(getApplication(), jsonData.getString("message") , Toast.LENGTH_LONG).show();
            } else {

                jsonArray = new JSONArray(jsonData.getString("class"));
                System.out.println("JSONARRAAY " + jsonArray);

                generateClassList(jsonArray);

                // update list using notifier
                classListAdapter.updateList(classListArrayList);
                classListAdapter.notifyDataSetChanged();

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onEventFailed() {

    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("ON RESUME");
        System.out.println("NOTIIIIIIIIIIIIIFY ");
        retrieveData();
    }

    protected void retrieveData() {
        MainClient mainClient = new MainClient(generatePostData(), server.getShowAllClassesAction(), this);
        mainClient.sendHttpRequest();
    }

    @Override
    protected JSONObject generatePostData() {
        postData = new JSONObject();
        try {
            postData.put("user_email", getUserEmail());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return postData;
    }

    protected void generateClassList(JSONArray jsonArray) {
        classListArrayList = new ArrayList<ClassList>();

        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = new JSONObject(jsonArray.getString(i));
                ClassList classList = new ClassList();
                classList.setClassId(jsonObject.getString("class_id"));
                classList.setClassName(jsonObject.getString("class_name"));
                classList.setLocation(jsonObject.getString("location"));
                classList.setTime(jsonObject.getString("time"));
                classList.setDate(jsonObject.getString("date"));
                classListArrayList.add(classList);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
