package com.fitchoice.mobile.admin;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.fitchoice.mobile.Adapter.ProfileListAdapter;
import com.fitchoice.mobile.R;
import com.fitchoice.mobile.object.Profile;

import java.util.ArrayList;

public class AdminSearchTrainerActivity extends BaseSearchActivity {

    EditText searchTrainerTxt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_search_trainer);
        setUserType(TRAINER);

        searchTrainerTxt = (EditText) findViewById(R.id.adminSearchTrainer);
        searchTrainerTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filterSearchItem(s.toString());
            }
        });

        setForwardOnEditIntent(new Intent(this, AdminEditTrainerActivity.class));

        profileArrayList = new ArrayList<Profile>();

        RecyclerView recyclerView = findViewById(R.id.recyclerViewAdminTrainer);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        profileListAdapter = new ProfileListAdapter(this, profileArrayList);
        profileListAdapter.setClickListener(this);
        recyclerView.setAdapter(profileListAdapter);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(), linearLayoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
    }

}