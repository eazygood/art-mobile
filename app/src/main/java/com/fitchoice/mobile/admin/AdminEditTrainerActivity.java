package com.fitchoice.mobile.admin;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.fitchoice.mobile.R;

import org.json.JSONException;
import org.json.JSONObject;

public class AdminEditTrainerActivity extends BaseSearchEditActivity {

    String userEmail;

    EditText nameTxt, surnameTxt, ageTxt, genderTxt, emailTxt;
    Button btnAdminTrainerConfirm, btnAdminTrainerCancel;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_admin_edit_trainer);
        userEmail = getUserEmail();

        String name = getIntent().getExtras() != null ?  getIntent().getExtras().getString("name") : "";
        String surName = getIntent().getExtras() != null ?  getIntent().getExtras().getString("surname") : "";
        String age = getIntent().getExtras() != null ?  getIntent().getExtras().getString("age") : "";
        String gender = getIntent().getExtras() != null ?  getIntent().getExtras().getString("gender") : "";
        String email = getIntent().getExtras() != null ?  getIntent().getExtras().getString("user_email") : "";

        nameTxt = (EditText) findViewById(R.id.trainerName);
        nameTxt.setText(name);

        surnameTxt = (EditText) findViewById(R.id.trainerSurname);
        surnameTxt.setText(surName);

        ageTxt = (EditText) findViewById(R.id.trainerAge);
        ageTxt.setText(age);

        genderTxt = (EditText) findViewById(R.id.trainerGender);
        genderTxt.setText(gender);

        emailTxt = (EditText) findViewById(R.id.trainerEmail);
        emailTxt.setText(email);
        emailTxt.setEnabled(false);
        emailTxt.setFocusable(false);

        btnAdminTrainerConfirm = (Button) findViewById(R.id.btnAdminTrainerConfirm);
        btnAdminTrainerConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onConfirmChanges();
            }
        });

        btnAdminTrainerCancel = (Button) findViewById(R.id.btnAdminTrainerCancel);
        btnAdminTrainerCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forwardBack();
            }
        });

        super.onCreate(savedInstanceState);

    }

    @Override
    protected JSONObject generatePostData() {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("email", emailTxt.getText());
            jsonObject.put("first_name", nameTxt.getText());
            jsonObject.put("last_name", surnameTxt.getText());
            jsonObject.put("age", ageTxt.getText());
            jsonObject.put("gender", genderTxt.getText());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }
}
