package com.fitchoice.mobile.Type;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.fitchoice.mobile.MainClient;
import com.fitchoice.mobile.R;

import org.json.JSONException;
import org.json.JSONObject;

public class ClientMyClassListEditActivity extends BaseClassListEditActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setContentView(R.layout.activity_client_cancel_class);
        classId = getIntent().getExtras() != null ?  getIntent().getExtras().getString("class_id") : "";
        userEmail = getUserEmail();
        System.out.println("UUUUUSER" + getUserEmail());

        String className = getIntent().getExtras() != null ?  getIntent().getExtras().getString("class_name") : "";
        String location = getIntent().getExtras() != null ?  getIntent().getExtras().getString("location") : "";
        String time = getIntent().getExtras() != null ?  getIntent().getExtras().getString("time") : "";
        String date = getIntent().getExtras() != null ?  getIntent().getExtras().getString("date") : "";

        editClassNameTxt = (EditText) findViewById(R.id.cancelClientClass);
        editClassNameTxt.setText(className);
        editLocationTxt = (EditText) findViewById(R.id.cancelClientLocation);
        editLocationTxt.setText(location);
        editTimeTxt = (EditText) findViewById(R.id.cancelClientTime);
        editTimeTxt.setText(time);
        editTimeTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickPickTime();
            }
        });
        editDateTxt = (EditText) findViewById(R.id.cancelClientDate);
        editDateTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickPickDate();
            }
        });

        editDateTxt.setText(date);

        btnSaveEditedClass = findViewById(R.id.btnClientCancel);
        btnSaveEditedClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (preRegistrationCheck()) {
                    onRegister();
                } else {
                    Toast.makeText(getApplication(), "Data not registered" , Toast.LENGTH_SHORT).show();
                }
            }
        });
        super.onCreate(savedInstanceState);

    }

    @Override
    protected JSONObject generatePostData() {
        postData = new JSONObject();
        try {
            postData.put("user_email", userEmail);
            postData.put("booked_class_id", classId);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return postData;
    }

    @Override
    protected void onRegister() {
        MainClient mainClient = new MainClient(generatePostData(), server.getCancelBookedClassAction(), this);
        mainClient.sendHttpRequest();
    }

    @Override
    protected String onSuccessToastMessage() {
        return "Class canceled";
    }
}
