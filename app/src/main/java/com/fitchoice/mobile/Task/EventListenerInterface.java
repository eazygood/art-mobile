package com.fitchoice.mobile.Task;

public interface EventListenerInterface {
    void onEventCompleted(String result);
    void onEventFailed();
}
