package com.fitchoice.mobile;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.fitchoice.mobile.Task.EventListenerInterface;
import com.fitchoice.mobile.admin.AdminHomeActivity;
import com.fitchoice.mobile.util.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class RegistrationActivity extends AppCompatActivity implements EventListenerInterface {

    RadioGroup radioGroupType;
    RadioGroup radioGroupGender;

    RadioButton selectedType;
    RadioButton selectedGender;

    RadioButton selectedMale;
    RadioButton selectedFemale;
    RadioButton selectedClientType;
    RadioButton selectedTrainerType;

    public static final String CLIENT = "Client";
    public static final String TRAINER = "Trainer";
    public static final String GENDER_MALE = "Male";
    public static final String GENDER_FEMALE = "Female";

    EditText firstName, lastName, personAge, email, password, rePassword;

    Boolean proceedForm = true;

    Button btnSignUp;
    TextView signIn;
    Server server = new Server();

    private JSONObject postData = new JSONObject();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        selectedClientType = findViewById(R.id.radClient);
        selectedTrainerType = findViewById(R.id.radTrainer);
        addRadioButtonColorListener(selectedClientType, selectedTrainerType);

        selectedMale = findViewById(R.id.radMale);
        selectedFemale = findViewById(R.id.radFemale);
        addRadioButtonColorListener(selectedMale, selectedFemale);

        // form fields
        firstName = findViewById(R.id.forename);
        lastName = findViewById(R.id.surname);
        personAge = findViewById(R.id.age);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        rePassword = findViewById(R.id.rePassword);

        radioGroupType = findViewById(R.id.radType);
        radioGroupGender = findViewById(R.id.radGender);

        btnSignUp = (Button) findViewById(R.id.btnSignUp);
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addEventListenerOnRadioButton();
                if (preRegistrationCheck()) {
                    System.out.println("PROCEEEEEEEEEEEEED" + generatePostData().toString());
                    onRegister();
                } else {
                    System.out.println("NOT PROCEEEEEEEEEEEED");
                }
            }
        });

        signIn = (TextView) findViewById(R.id.signIn);
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backToLoginActivity();
            }
        });
    }

    private void addEventListenerOnRadioButton(){

        int selectedTypeId =  radioGroupType.getCheckedRadioButtonId();
        selectedType = (RadioButton) findViewById(selectedTypeId);

        int selectedGenderId = radioGroupGender.getCheckedRadioButtonId();
        selectedGender = (RadioButton) findViewById(selectedGenderId);
    }

    private Boolean preRegistrationCheck() {
        isEmpty(firstName);
        isEmpty(lastName);
        isEmpty(personAge);
        isEmpty(email);
        isEmpty(password);
        isEmpty(rePassword);

        return proceedForm;
    }

    private void isEmpty(EditText text) {

        if (text.getText().toString().trim().equals("")) {
            text.setError("Required field");
            proceedForm = false;
        } else {
            proceedForm = true;
        }
    }

    private void onRegister() {
        MainClient mainClient = new MainClient(generatePostData(), server.getRegister(), this);
        mainClient.sendHttpRequest();
    }

    private void addRadioButtonColorListener(final RadioButton radioBtn, final RadioButton radioBtn2) {

        radioBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRadioButtonTextColor(radioBtn, radioBtn2);
            }
        });

        radioBtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setRadioButtonTextColor(radioBtn2, radioBtn);
            }
        });
    }

    protected void backToLoginActivity() {
        Intent in = new Intent(this, LoginActivity.class);
        startActivity(in);
    }

    public void setRadioButtonTextColor(RadioButton radioButton, RadioButton radioButton2) {

        if (radioButton.isChecked()) {
            radioButton.setTextColor(Color.WHITE);
            radioButton2.setTextColor(Color.BLACK);
        } else {
            radioButton.setTextColor(Color.BLACK);
            radioButton2.setTextColor(Color.WHITE);
        }
    }

    protected Integer getRadioButtonColor(RadioButton radioButton) {
        if (radioButton.getTextColors().getDefaultColor() == Color.WHITE) {
            return Color.BLACK;
        } else {
            return Color.WHITE;
        }
    }

    protected void signUpAction() {

    }

    protected JSONObject generatePostData() {
        try {
            Boolean type = false;
            String gender = "m";
            int age = Integer.parseInt(personAge.getText().toString().trim());

            if (selectedType.getText().toString().trim().equals(TRAINER)) {
                type = true;
            }

            if (selectedGender.getText().toString().trim().equals(GENDER_FEMALE)) {
                gender = "f";
            }

            postData.put("first_name", firstName.getText().toString().trim());
            postData.put("last_name", lastName.getText().toString().trim());
            postData.put("is_trainer", type);
            postData.put("gender", gender);
            postData.put("email", email.getText().toString().trim());
            postData.put("age", age);
            postData.put("password", password.getText().toString().trim());
            postData.put("re_password", rePassword.getText().toString().trim());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return postData;
    }

    @Override
    public void onEventCompleted(String result) {
        System.out.println("on_complete" + result);

        try {
            JSONObject jsonObject = new JSONObject(result);

            String jsonStatus;
            jsonStatus = jsonObject.getString("status");

            System.out.println("json4ik" + jsonStatus);
            if (jsonStatus.equals("false")) {
                JSONObject jsonData = jsonObject.getJSONObject("data");
                System.out.println(jsonData.getString("message"));
                Toast.makeText(getApplication(), jsonData.getString("message") , Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplication(), "User created" , Toast.LENGTH_SHORT).show();
                Intent in = new Intent(this, LoginActivity.class);
                startActivity(in);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onEventFailed() {

    }
}
