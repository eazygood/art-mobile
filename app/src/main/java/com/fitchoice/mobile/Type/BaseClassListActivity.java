package com.fitchoice.mobile.Type;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.Toast;

import com.fitchoice.mobile.Adapter.ClassListAdapter;
import com.fitchoice.mobile.object.ClassList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class BaseClassListActivity extends AbstractTypeActivity implements ClassListAdapter.ItemClickListener{

    ArrayList<ClassList> classListArrayList;

    ClassListAdapter classListAdapter;
    LinearLayoutManager linearLayoutManager;
    JSONArray jsonArray;

    Intent intent;

    @Override
    public void onItemClick(View view, int position) {
        Toast.makeText(this, "You clicked  on row number " + position, Toast.LENGTH_SHORT).show();
        forwardOnEditClass(position);
    }

    @Override
    public void onEventCompleted(String result) {
        System.out.println("on_complete" + result);

        try {
            JSONObject jsonObject = new JSONObject(result);

            String jsonStatus;
            jsonStatus = jsonObject.getString("status");

            System.out.println("json4ik" + jsonStatus);
            JSONObject jsonData = jsonObject.getJSONObject("data");

            if (jsonStatus.equals("false")) {
                System.out.println(jsonData.getString("message"));
                Toast.makeText(getApplication(), jsonData.getString("message") , Toast.LENGTH_LONG).show();
            } else {

                jsonArray = new JSONArray(jsonData.getString("class"));
                System.out.println("JSONARRAAY " + jsonArray);

                generateClassList(jsonArray);

                // update list using notifier
                System.out.println(classListArrayList.size() + " SIIIIIIIIIIIZE");
                if (classListArrayList.size() > 0) {
                    classListAdapter = getClassListAdapter();
                    classListAdapter.updateList(classListArrayList);
                    classListAdapter.notifyDataSetChanged();
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onEventFailed() {

    }

    protected void forwardOnEditClass(int position) {
        Intent in = this.getForwardOnEditClassIntent();

        ClassList classList = classListAdapter.getClassListItem(position);
        in.putExtra("email", getUserEmail());
        in.putExtra("class_id", classList.getClassId());
        in.putExtra("class_name", classList.getClassName());
        in.putExtra("location", classList.getLocation());
        in.putExtra("time", classList.getTime());
        in.putExtra("date", classList.getDate());

        startActivity(in);
    }

    protected Intent getForwardOnEditClassIntent() {
        return intent;
    }

    protected void setForwardOnEditClassIntent(Intent in) {
        intent = in;
    }

    protected void generateClassList(JSONArray jsonArray) {
        classListArrayList = new ArrayList<ClassList>();

        try {

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = new JSONObject(jsonArray.getString(i));
                ClassList classList = new ClassList();
                classList.setClassId(jsonObject.getString("class_id"));
                classList.setClassName(jsonObject.getString("class_name"));
                classList.setLocation(jsonObject.getString("location"));
                classList.setTime(jsonObject.getString("time"));
                classList.setDate(jsonObject.getString("date"));
                classListArrayList.add(classList);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected JSONObject generatePostData() {
        postData = new JSONObject();
        try {
            postData.put("user_email", getUserEmail());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return postData;
    }

    public void setClassListAdapter(ClassListAdapter adapter) {
        classListAdapter = adapter;
    }

    public ClassListAdapter getClassListAdapter()
    {
        return classListAdapter;
    }

}
