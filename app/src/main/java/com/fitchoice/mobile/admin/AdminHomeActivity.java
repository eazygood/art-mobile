package com.fitchoice.mobile.admin;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.fitchoice.mobile.R;
import com.fitchoice.mobile.Type.AbstractTypeActivity;

public class AdminHomeActivity extends AbstractTypeActivity {

    Button btnAdmSearchClient, btnAdmSearchTrainer, btnAdmSearchClass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.activity_admin_home);
        setWelcomeTextView();

        // set buttons
        btnAdmSearchClient = (Button) findViewById(R.id.btnAdmSearchClient);
        btnAdmSearchClient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forwardOnSearchClient();
            }
        });
        btnAdmSearchTrainer = (Button) findViewById(R.id.btnAdmSearchTrainer);
        btnAdmSearchTrainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forwardOnSearchTrainer();
            }
        });
        btnAdmSearchClass = (Button) findViewById(R.id.btnAdmSearchClass);
        btnAdmSearchClass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forwardOnSearchClass();
            }
        });
        super.onCreate(savedInstanceState);

    }

    @Override
    protected TextView getWelcomeView() {
        return (TextView) findViewById(R.id.welcomeNameAdmin);
    }

    protected void forwardOnSearchClient()
    {
        Intent in = new Intent(this, AdminSearchClientActivity.class);
        in.putExtra("email", getUserEmail());
        startActivity(in);
    }

    protected void forwardOnSearchTrainer()
    {
        Intent in = new Intent(this, AdminSearchTrainerActivity.class);
        in.putExtra("email", getUserEmail());
        startActivity(in);
    }

    protected void forwardOnSearchClass()
    {
        Intent in = new Intent(this, AdminSearchClassActivity.class);
        in.putExtra("email", getUserEmail());
        startActivity(in);
    }

    @Override
    public void onEventCompleted(String result) {

    }

    @Override
    public void onEventFailed() {

    }
}


