package com.fitchoice.mobile.admin;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.fitchoice.mobile.Adapter.ProfileListAdapter;
import com.fitchoice.mobile.MainClient;
import com.fitchoice.mobile.Type.AbstractTypeActivity;
import com.fitchoice.mobile.object.ClassList;
import com.fitchoice.mobile.object.Profile;
import com.fitchoice.mobile.util.Server;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class BaseSearchActivity extends AbstractTypeActivity implements ProfileListAdapter.ItemClickListener {

    ArrayList<Profile> profileArrayList;
    ProfileListAdapter profileListAdapter;
    LinearLayoutManager linearLayoutManager;
    JSONObject postData;
    JSONArray jsonArray;
    Intent intent;

    Server server = getServer();

    static final String CLIENT = "CLIENT";
    static final String TRAINER = "TRAINER";

    String userType;

    @Override
    public void onItemClick(View view, int position) {
        forwardOnEditClass(position);
    }

    @Override
    public void onEventCompleted(String result) {

        System.out.println("on_complete" + result);

        try {
            JSONObject jsonObject = new JSONObject(result);

            String jsonStatus;
            jsonStatus = jsonObject.getString("status");

            System.out.println("json4ik" + jsonStatus);
            JSONObject jsonData = jsonObject.getJSONObject("data");

            if (jsonStatus.equals("false")) {
                System.out.println(jsonData.getString("message"));
                Toast.makeText(getApplication(), jsonData.getString("message") , Toast.LENGTH_LONG).show();
            } else {

                jsonArray = new JSONArray(jsonData.getString("users"));
                System.out.println("JSONARRAAY " + jsonArray);

                generateProfileList(jsonArray);

                // update list using notifier
                profileListAdapter.updateList(profileArrayList);
                profileListAdapter.notifyDataSetChanged();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onEventFailed() {

    }

    protected void generateProfileList(JSONArray jsonArray) {
        profileArrayList = new ArrayList<Profile>();

            try {
            for (int i = 0; i < jsonArray.length(); ++i) {
                JSONObject jsonObject = new JSONObject(jsonArray.getString(i));
                Profile profile = new Profile();
                System.out.println(jsonArray.getString(i));
                int userId = Integer.parseInt(jsonObject.getString("user_id"));
                int age = Integer.parseInt(jsonObject.getString("age"));

                profile.setUserId(userId);
                profile.setEmail(jsonObject.getString("email"));
                profile.setName(jsonObject.getString("first_name"));
                profile.setSurname(jsonObject.getString("last_name"));
                profile.setAge(age);
                profile.setGender(jsonObject.getString("gender"));
                profileArrayList.add(profile);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected void forwardOnEditClass(int position) {
        Intent in = this.getForwardOnEditIntent();

        Profile profile = profileListAdapter.getProfileItem(position);
        in.putExtra("user_id", profile.getUserId());
        in.putExtra("name", profile.getName());
        in.putExtra("surname", profile.getSurname());
        in.putExtra("email", getUserEmail());
        in.putExtra("user_email", profile.getEmail());
        in.putExtra("gender", profile.getGender());
        in.putExtra("age", profile.getAge().toString());

        startActivity(in);
    }

    protected Intent getForwardOnEditIntent() {
        return intent;
    }

    protected void setForwardOnEditIntent(Intent in) {
        intent = in;
    }

    protected void filterSearchItem(String text) {
        ArrayList<Profile> filteredList = new ArrayList<>();

        for (Profile item : profileArrayList) {
            if (item.getName().toLowerCase().contains(text.toLowerCase())) {
                filteredList.add(item);
            }
        }

        profileListAdapter.filterList(filteredList);
    }

    @Override
    protected JSONObject generatePostData() {
        postData = new JSONObject();
        try {
            postData.put("user_email", getUserEmail());
            postData.put("type", getUserType());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return postData;
    }

    protected void setUserType(String type) {
        if (type.equals(CLIENT)) {
            userType = CLIENT;
        } else if (type.equals(TRAINER)) {
            userType = TRAINER;
        }
    }

    public String getUserType() {
        return userType;
    }

    protected void retrieveData() {
        MainClient mainClient = new MainClient(generatePostData(), server.getShowUsersAction(), this);
        mainClient.sendHttpRequest();
    }

    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("ON RESUME");
        System.out.println("ADMIN ACTIVITY " + getUserEmail());
        retrieveData();
    }

}
